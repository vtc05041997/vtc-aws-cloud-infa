
variable "environment" {
  description = "Evironment for the project"
  type        = string
}

# //
# // Tags
# //
variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
}

variable "branch_name" {
  description = "branch name"
  type        = string
}

variable "raw_bucket_name" {
  description = "raw_bucket_name"
  type        = string
}

variable "name" {
  type        = string
  description = "Name project codebuild"
}

variable "codebuild_type" {
  type        = string
  description = "codebuild_type"
}

variable "codebuild_role_arn" {
  type        = string
  description = "codebuild_role_arn"
}

variable "buildspec" {
  type        = string
  description = "buildspec"
}

variable "codebuild_general1_small_compute_type" {
  type        = string
  description = "codebuild_general1_small_compute_type for codebuild"
}

variable "codebuild_image" {
  type        = string
  description = "codebuild_image"
}

variable "codebuild_container_type" {
  type        = string
  description = "codebuild_container_type"
}

variable "codebuild_image_pull_credentials_type" {
  type        = string
  description = "codebuild_image_pull_credentials_type"
}

variable "codepipeline_role_arn" {
  type        = string
  description = "codepipeline_role_arn"
}

variable "image_repo_name" {
  type = string
}

variable "image_tag" {
  type = string
}