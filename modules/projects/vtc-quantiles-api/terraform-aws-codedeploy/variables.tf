variable "name" {
  type = string
}

variable "lb_target_group" {
  type = string
}

variable "auto_scaling_groups" {
  type = set(string)
}

variable "tags" {
  type = map(string)
}

variable "codedeploy_role_arn" {
  type = string
}

variable "application_type" {
  type = string
}