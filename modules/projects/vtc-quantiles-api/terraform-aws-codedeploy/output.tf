output "aws_codedeploy_app_name" {
  value = aws_codedeploy_app.api.name
}

output "aws_codedeploy_app_id" {
  value = aws_codedeploy_app.api.id
}

output "aws_codedeploy_app_arn" {
  value = aws_codedeploy_app.api.arn
}

output "aws_codedeploy_deployment_group_name" {
  value = aws_codedeploy_deployment_group.api_group.deployment_group_name
}
