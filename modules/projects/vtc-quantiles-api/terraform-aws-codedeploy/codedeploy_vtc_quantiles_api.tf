resource "aws_codedeploy_app" "api" {
  name = format("%s-cd", var.name)
}

# create a deployment group
resource "aws_codedeploy_deployment_group" "api_group" {
  app_name              = aws_codedeploy_app.api.name
  deployment_group_name = format("%s-cd-grp", var.name)
  service_role_arn      = var.codedeploy_role_arn

  deployment_config_name = "CodeDeployDefault.OneAtATime" # AWS defined deployment config

  deployment_style {
    deployment_option = "WITHOUT_TRAFFIC_CONTROL" # Default, if use WITH_TRAFFIC_CONTROL for loadbalance
    deployment_type   = "IN_PLACE"
  }


  # trigger a rollback on deployment failure event
  auto_rollback_configuration {
    enabled = true
    events = [
      "DEPLOYMENT_FAILURE",
    ]
  }
  # ec2_tag_set { 
  #   ec2_tag_filter {
  #     key   = "Name"
  #     type  = "KEY_AND_VALUE"
  #     value = format("ASG-instance-%s", var.application_type)
  #   }
  # }

  # blue_green_deployment_config {
  #   deployment_ready_option {
  #     action_on_timeout    = "STOP_DEPLOYMENT" # The value here was "CONTINUE_DEPLOYMENT" before I change
  #     wait_time_in_minutes = 30                # did not set before I change
  #   }

  #   terminate_blue_instances_on_deployment_success {
  #     action                           = "TERMINATE"
  #     termination_wait_time_in_minutes = 30
  #   }
  # }

  #user loadbalance for blue/green

  # load_balancer_info {
  #   target_group_info {
  #     name = var.lb_target_group
  #   }
  # }
  autoscaling_groups = var.auto_scaling_groups
}