# Code commit
resource "aws_codecommit_repository" "repo_vtc_quantiles_api" {
  repository_name = format("%s-code-comm", var.name)
  description     = "CodeCommit repo for ${var.name}"
  # default_branch  = "develop"
}

#### Build & Deploy ####
resource "aws_codebuild_project" "codebuild_vtc_quantiles_api" {
  name           = format("%s-code-bld", var.name)
  description    = "codebuild project for ${var.name} repo"
  build_timeout  = "30"
  queued_timeout = "60"

  service_role = var.codebuild_role_arn

  artifacts {
    type = var.codebuild_type
  }

  cache {
    type     = "S3"
    location = format("%s/cache/%s", var.raw_bucket_name, var.name)
  }

  environment {
    compute_type                = var.codebuild_general1_small_compute_type
    image                       = var.codebuild_image
    type                        = var.codebuild_container_type
    image_pull_credentials_type = var.codebuild_image_pull_credentials_type
    privileged_mode             = false

    environment_variable {
      name  = "NAME"
      value = var.name
    }

    environment_variable {
      name  = "AWS_ACCOUNT_ID"
      value = data.aws_caller_identity.current.account_id
    }

  }

  logs_config {
    cloudwatch_logs {
    }

    s3_logs {
      status   = "ENABLED"
      location = "${var.raw_bucket_name}/build-log/${var.name}"
    }
  }

  source {
    type      = var.codebuild_type
    buildspec = var.buildspec
  }

  tags = merge(
    {
      "Name" = format("%s-code-bld", var.name)
    },
    var.tags
  )
}

# CICD Pipeline
resource "aws_codepipeline" "codepipelines_vtc_quantiles_api" {
  name     = format("%s-code-pipe", var.name)
  role_arn = var.codepipeline_role_arn

  artifact_store {
    location = var.raw_bucket_name
    type     = "S3"
  }

  tags = merge(
    {
      "Name" = format("%s-code-pipe", var.name)
    },
    var.tags
  )

  stage {
    name = "Source"

    action {
      name             = "source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeCommit"
      version          = "1"
      namespace        = "SourceVariables"
      output_artifacts = ["source_output"]

      configuration = {
        RepositoryName       = aws_codecommit_repository.repo_vtc_quantiles_api.repository_name
        BranchName           = var.branch_name
        OutputArtifactFormat = "CODE_ZIP"
        PollForSourceChanges = false
      }
    }
  }

  dynamic "stage" {
    for_each = var.environment == "production" ? [var.environment] : []

    content {
      name = "Approve"

      action {
        category         = "Approval"
        configuration    = {}
        input_artifacts  = []
        name             = "manual_approval"
        output_artifacts = []
        owner            = "AWS"
        provider         = "Manual"
        version          = "1"
      }
    }
  }

  stage {
    name = "Build"
    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      input_artifacts  = ["source_output"]
      output_artifacts = ["deploy_output"]
      version          = "1"
      namespace        = "BuildVariables"

      configuration = {
        ProjectName   = aws_codebuild_project.codebuild_vtc_quantiles_api.name
        PrimarySource = "source_output"
      }
    }
  }

  stage {
    name = "Deploy"
    action {
      name            = "Deploy"
      category        = "Deploy"
      owner           = "AWS"
      provider        = "CodeDeploy"
      version         = "1"
      input_artifacts = ["deploy_output"]
      namespace       = "DeployVariables"
      configuration = {
        ApplicationName     = var.codedeploy_app_name,
        DeploymentGroupName = var.codedeploy_deployment_group_name,
      }
    }
  }
}

# CloudWatch Event Resources
resource "aws_iam_role" "cwe_role_vtc_quantiles_api" {
  name = format("%s-iam-cwe-codeppl-role", var.name)

  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        Effect : "Allow",
        Principal : {
          "Service" : "events.amazonaws.com"
        },
        Action : "sts:AssumeRole"
      }
    ]
  })

  tags = merge(
    var.tags
  )

  inline_policy {
    name = "start_pipeline_policy"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = ["codepipeline:StartPipelineExecution"]
          Effect   = "Allow"
          Resource = aws_codepipeline.codepipelines_vtc_quantiles_api.arn
        },
      ]
    })
  }
}

resource "aws_cloudwatch_event_rule" "cwe_vtc_quantiles_api" {
  name        = format("%s-%s", var.name, "cwe-codeppl")
  description = format("Event rule for triggering codepipeline on push to repo %s on %s env", var.name, upper(var.environment))
  event_pattern = jsonencode(
    {
      "detail-type" : ["CodeCommit Repository State Change"],
      "source" : ["aws.codecommit"],
      "resources" : [aws_codecommit_repository.repo_vtc_quantiles_api.arn],
      "detail" : {
        "event" : ["referenceUpdated"],
        "repositoryName" : [aws_codecommit_repository.repo_vtc_quantiles_api.repository_name],
        "referenceType" : ["branch"],
        "referenceName" : [var.branch_name]
      }
  })
  tags = merge(
    var.tags
  )
}

resource "aws_cloudwatch_event_target" "vtc_quantiles_api" {
  rule     = aws_cloudwatch_event_rule.cwe_vtc_quantiles_api.name
  arn      = aws_codepipeline.codepipelines_vtc_quantiles_api.arn
  role_arn = aws_iam_role.cwe_role_vtc_quantiles_api.arn
}


#CodeDeploy For Application Deploy on EC2 or LoadBalancer