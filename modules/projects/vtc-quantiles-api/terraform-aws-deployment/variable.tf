variable "name" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "public_subnets" {
  type = list(string)
}

variable "instances" {
  type = list(string)
}

# variable "map_name_id_instances_api" {
#   type = map(string)
# }

variable "tags" {
  type = map(string)
}

variable "private_subnets" {
  type = list(string)
}

variable "instance_keypair" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "image_id" {
  type = string
}

variable "security_groups" {
  type = set(string)
}

variable "application_type" {
  type = string
}

variable "aws_iam_instance_profile_arn" {
  type = string
}