resource "aws_lb" "alb" {
  name               = format("%s-alb", var.name)
  internal           = false
  load_balancer_type = "application"
  security_groups    = var.security_groups
  idle_timeout       = 60
  subnets            = var.public_subnets

  #enable_cross_zone_load_balancing = true alway ON alb no charge, off nlb and charge if on

  tags = merge(
    {
      "Name" = format("%s-abl", var.name)
    },
    var.tags
  )
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "8101"
  protocol          = "HTTP"

  # ssl_policy        = "ELBSecurityPolicy-2016-08"
  # certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg.arn
  }
}

resource "aws_lb_target_group" "tg" {
  name     = format("%s-tg", var.name)
  port     = 8101
  protocol = "HTTP"
  vpc_id   = var.vpc_id
  health_check {
    enabled             = true
    path                = "/"
    port                = "80"
    protocol            = "HTTP"
    healthy_threshold   = 3
    unhealthy_threshold = 2
    interval            = 30
    timeout             = 10
    matcher             = "200"
  }

  # depends_on = [aws_lb.alb]

  tags = merge(
    {
      "Name" = format("%s-tg", var.name)
    },
    var.tags
  )
}

resource "aws_lb_cookie_stickiness_policy" "this" {
  name                     = format("%s-alb-sp", var.name)
  load_balancer            = aws_lb.alb.id
  lb_port                  = 80
  cookie_expiration_period = 600
}

## For intances
# resource "aws_lb_target_group_attachment" "attachment" {
#   count            = length(var.instances)
#   target_group_arn = aws_lb_target_group.tg.arn
#   target_id        = var.map_name_id_instances_api[element(var.instances, count.index)]
#   port             = 80
# }


## For elb
# resource "aws_lb_target_group_attachment" "attachment" {
#   target_group_arn = aws_lb_target_group.tg.arn
#   target_id        = aws_autoscaling_group.my_asg.id
#   port             = 80

#   depends_on = [ aws_autoscaling_group.my_asg, aws_lb_target_group.tg]

# }

