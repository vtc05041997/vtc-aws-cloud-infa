output "lb_target_group" {
  value = aws_lb_target_group.tg.name
}

output "auto_scaling_groups" {
  value = aws_autoscaling_group.my_asg.name
}