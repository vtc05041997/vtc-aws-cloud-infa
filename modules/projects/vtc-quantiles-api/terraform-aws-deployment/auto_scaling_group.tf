resource "aws_launch_template" "my_launch_template" {
  name          = format("%s-ec2-template", var.name)
  image_id      = var.image_id
  instance_type = var.instance_type

  vpc_security_group_ids = var.security_groups
  key_name               = var.instance_keypair
  iam_instance_profile {
    arn = var.aws_iam_instance_profile_arn
  }

  user_data = filebase64("ec2-user-data.sh")

  # ebs_optimized = true
  #default_version = 1
  # update_default_version = true
  # block_device_mappings {
  #   device_name = "/dev/sda1"
  #   ebs {
  #     volume_size = 10
  #     #volume_size = 20 # LT Update Testing - Version 2 of LT      
  #     delete_on_termination = true
  #     volume_type           = "gp2" # default is gp2
  #   }
  # }
  # monitoring {
  #   enabled = true
  # }

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = format("ASG-instance-%s", var.application_type)
    }
  }
}

# Autoscaling Group Resource
resource "aws_autoscaling_group" "my_asg" {
  name_prefix         = format("%s-asg", var.name)
  desired_capacity    = 1
  max_size            = 2
  min_size            = 1
  vpc_zone_identifier = var.public_subnets

  health_check_type = "ELB"
  #health_check_grace_period = 300 # default is 300 seconds  
  # Launch Template
  launch_template {
    id      = aws_launch_template.my_launch_template.id
    version = aws_launch_template.my_launch_template.latest_version
  }
  # Instance Refresh
  instance_refresh {
    strategy = "Rolling"
    preferences {
      #instance_warmup = 300 # Default behavior is to use the Auto Scaling Group's health check grace period.
      min_healthy_percentage = 50
    }
    triggers = [/*"launch_template",*/ "desired_capacity"] # You can add any argument from ASG here, if those has changes, ASG Instance Refresh will trigger
  }
  tag {
    key                 = "Application"
    value               = var.application_type
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_attachment" "attachment" {
  autoscaling_group_name = aws_autoscaling_group.my_asg.id
  lb_target_group_arn    = aws_lb_target_group.tg.arn

  depends_on = [aws_autoscaling_group.my_asg, aws_lb_target_group.tg]
}