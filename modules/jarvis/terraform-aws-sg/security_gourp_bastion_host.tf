resource "aws_security_group" "security_group_bastion_host" {
  name        = format("%s-sg", var.name_bastion_host_sg)
  description = "Allow http inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    {
      "Name" = format("%s-sg", var.name_bastion_host_sg)
    },
    var.tags
  )
}