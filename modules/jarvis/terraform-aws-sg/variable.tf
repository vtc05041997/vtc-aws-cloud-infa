variable "name_bastion_host_sg" {
  type        = string
  description = "name of the security group for bastion hosts"
}

variable "name_api_sg" {
  type        = string
  description = "name of the security group for api"
}

variable "tags" {
  type = map(string)
}

variable "vpc_id" {
  type = string
}