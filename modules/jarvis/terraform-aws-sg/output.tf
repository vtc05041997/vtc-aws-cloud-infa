output "security_group_api_id" {
  value = aws_security_group.security_group_api.id
}

output "security_group_api_arn" {
  value = aws_security_group.security_group_api.arn
}

output "security_group_bastion_host_id" {
  value = aws_security_group.security_group_bastion_host.id
}

output "security_group_bastion_host_arn" {
  value = aws_security_group.security_group_bastion_host.arn
}

output "map_security_group_name_id" {
  value = {
    "${aws_security_group.security_group_bastion_host.name}" = aws_security_group.security_group_bastion_host.id,
    "${aws_security_group.security_group_api.name}"          = aws_security_group.security_group_api.id
  }
}