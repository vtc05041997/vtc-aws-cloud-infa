resource "aws_security_group" "security_group_api" {
  name        = format("%s-sg", var.name_api_sg)
  description = "Allow http inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 8101
    to_port     = 8101
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    {
      "Name" = format("%s-sg", var.name_api_sg)
    },
    var.tags
  )
}