resource "aws_iam_role" "codedeploy_role" {
  name = format("%s-cd-role", var.name)
  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "codedeploy.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
  })
  tags = merge(
    {
      "Name" = format("%s-cd-role", var.name)
    },
    var.tags
  )
}

resource "aws_iam_role_policy_attachment" "codedeploy" {
  role       = aws_iam_role.codedeploy_role.name
  policy_arn = aws_iam_policy.codedeploy_default_policy.arn
}

resource "aws_iam_policy" "codedeploy_default_policy" {
  name = format("%s-cd-policy", var.name)
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "CodeDeployPermissions",
        "Effect" : "Allow",
        "Action" : [
          "codedeploy:Get*",
          "codedeploy:List*",
          "codedeploy:RegisterApplicationRevision",
          "codedeploy:CreateDeployment",
          "codedeploy:GetDeployment",
          "codedeploy:GetDeploymentConfig",
          "codedeploy:ListDeploymentConfigs",
          "codedeploy:ListDeploymentGroups",
          "codedeploy:GetDeploymentGroup",
          "codedeploy:CreateDeploymentConfig",
          "codedeploy:DeleteDeploymentConfig",
          "codedeploy:UpdateDeploymentGroup",
          "codedeploy:CreateDeploymentGroup",
          "codedeploy:DeleteDeploymentGroup",
          "codedeploy:BatchGet*",
          "codedeploy:CreateApplication",
          "codedeploy:DeleteApplication",
          "codedeploy:UpdateApplication",
          "codedeploy:ListApplications",
          "codedeploy:ListApplicationRevisions",
          "codedeploy:GetApplicationRevision",
          "codedeploy:DeleteApplicationRevision"
        ],
        "Resource" : "*"
      },
      {
        "Sid" : "S3Permissions",
        "Effect" : "Allow",
        "Action" : [
          "s3:Get*",
          "s3:List*"
        ],
        "Resource" : [
          "arn:aws:s3:::${var.raw_bucket_name}/*",
        ]
      },
      {
        "Sid" : "AutoscalingPermissions",
        "Effect" : "Allow",
        "Action" : [
          "autoscaling:CompleteLifecycleAction",
          "autoscaling:DeleteLifecycleHook",
          "autoscaling:DescribeAutoScalingGroups",
          "autoscaling:DescribeLifecycleHooks",
          "autoscaling:PutLifecycleHook",
          "autoscaling:RecordLifecycleActionHeartbeat"
        ],
        "Resource" : "*"
      },
      {
        "Effect" : "Allow",
        "Action" : [
          "sts:GetCallerIdentity",
          "ecr:GetAuthorizationToken",
          "ecr:BatchGetImage",
          "sqs:ListQueues",
          "ec2:DescribeSpotPriceHistory",
          "iam:PassRole",
          "ec2:RunInstances",
          "ec2:CreateTags",
          "ec2:DescribeInstances",
          "ec2:DescribeInstanceStatus"
        ],
        "Resource" : "*"
      },
      {
        "Action": [
            "ecs:DescribeServices",
            "ecs:CreateTaskSet",
            "ecs:UpdateServicePrimaryTaskSet",
            "ecs:DeleteTaskSet",
            "elasticloadbalancing:DescribeTargetGroups",
            "elasticloadbalancing:DescribeListeners",
            "elasticloadbalancing:ModifyListener",
            "elasticloadbalancing:DescribeRules",
            "elasticloadbalancing:ModifyRule",
            "lambda:InvokeFunction",
            "cloudwatch:DescribeAlarms",
            "sns:Publish",
            "s3:GetObject",
            "s3:GetObjectVersion"
        ],
        "Resource": "*",
        "Effect": "Allow"
        },
        {
            "Action": [
                "iam:PassRole"
            ],
            "Effect": "Allow",
            "Resource": "*",
            "Condition": {
                "StringLike": {
                    "iam:PassedToService": [
                        "ecs-tasks.amazonaws.com"
                    ]
                }
            }
        }
    ]
  })
}