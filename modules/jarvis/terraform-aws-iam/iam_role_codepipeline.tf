resource "aws_iam_role" "codepipeline_role" {
  name = format("%s-codepipeline-role", var.name)
  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "codepipeline.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
  })
  tags = merge(
    {
      "Name" = format("%s-codepipeline-role", var.name)
    },
    var.tags
  )
}

resource "aws_iam_role_policy_attachment" "codepipelines" {

  role       = aws_iam_role.codepipeline_role.name
  policy_arn = aws_iam_policy.codepipelines_default_policy.arn
}

resource "aws_iam_policy" "codepipelines_default_policy" {
  name = format("%s-codepipelines-policy", var.name)
  path = "/"
  # description = "Permission to execute code pipelines"
  tags = merge(
    var.tags
  )
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Action" : [
          "s3:PutObject",
          "s3:GetObject",
          "s3:GetObjectVersion",
          "s3:GetBucketAcl",
          "s3:GetBucketLocation",
          "s3:DeleteObject",
          "s3:ListBucket"
        ],
        "Effect" : "Allow",
        "Resource" : [
          "arn:aws:s3:::${var.raw_bucket_name}/*",
        ]
      },
      {
        "Action" : [
          "codecommit:CancelUploadArchive",
          "codecommit:GetBranch",
          "codecommit:GetCommit",
          "codecommit:GetRepository",
          "codecommit:GetUploadArchiveStatus",
          "codecommit:UploadArchive"
        ],
        "Effect" : "Allow"
        "Resource" : "*"
      },
      {
        "Action" : [
          "codebuild:BatchGetBuilds",
          "codebuild:StartBuild",
          "codebuild:BatchGetBuildBatches",
          "codebuild:StartBuildBatch"
        ],
        "Effect" : "Allow",
        "Resource" : "*"
      },
      {
        "Sid" : "AutoscalingPermissions",
        "Effect" : "Allow",
        "Action" : [
          "autoscaling:*"
        ],
        "Resource" : "*"
      },
      {
        "Action" : [
          "codedeploy:CreateDeployment",
          "codedeploy:GetApplication",
          "codedeploy:GetApplicationRevision",
          "codedeploy:GetDeployment",
          "codedeploy:GetDeploymentConfig",
          "codedeploy:RegisterApplicationRevision"
        ],
        "Resource" : "*",
        "Effect" : "Allow"
      }
    ]
  })

}