resource "aws_iam_role" "iam_sfn_role" {
  name = format("%s-sfn-role", var.name)

  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Action" : "sts:AssumeRole",
        "Principal" : {
          "Service" : "states.amazonaws.com"
        },
        "Effect" : "Allow",
        "Sid" : ""
      }
    ]
  })
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/AWSLambdaExecute",
    "arn:aws:iam::aws:policy/AmazonS3FullAccess",
    aws_iam_policy.iam_sfn_policy.arn
  ]

  tags = merge(
    var.tags
  )
}

resource "aws_iam_policy" "iam_sfn_policy" {
  name = format("%s-sfn-policy", var.name)
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "glue:StartJobRun",
          "glue:GetJobRun",
          "glue:GetJobRuns",
          "glue:BatchStopJobRun"
        ]
        "Resource" : "arn:aws:glue:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:*"
      },
      {
        "Effect" : "Allow",
        "Action" : [
          "logs:CreateLogDelivery",
          "logs:GetLogDelivery",
          "logs:UpdateLogDelivery",
          "logs:DeleteLogDelivery",
          "logs:ListLogDeliveries",
          "logs:PutResourcePolicy",
          "logs:DescribeResourcePolicies",
          "logs:DescribeLogGroups",
          "Lambda:InvokeFunction"
        ]
        "Resource" : "*"
      },
      {
        "Effect": "Allow",
        "Action": [
          "states:GetExecutionHistory",
          "states:StartExecution",
          "states:DescribeExecution",
          "states:StartSyncExecution",
          "states:DescribeStateMachine",
          "states:ListExecutions",
          "states:StopExecution"
        ],
        "Resource": [
          "*"
        ]
      }
    ]
  })
}