resource "aws_iam_role" "iam_glue_job_role" {
  name = format("%s-glue-job-role", var.name)

  assume_role_policy = jsonencode({
    "Version" : "2008-10-17",
    "Statement" : [
      {
        "Sid" : "",
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "glue.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
  })
  managed_policy_arns = [
    aws_iam_policy.iam_glue_job_policy.arn,
    aws_iam_policy.iam_glue_job_s3_policy.arn,
    # var.iam_readonly_kms_policy_arn,
    "arn:aws:iam::aws:policy/service-role/AWSGlueServiceRole"
  ]

  tags = merge(
    {
      "Name" = format("%s-glue-job-role", var.name)
    },
    var.tags
  )
}

resource "aws_iam_policy" "iam_glue_job_policy" {
  name        = format("%s-glue-job-policy", var.name)
  path        = "/"
  description = "This role used by the Glue Jobs"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "lakeformation:GetDataAccess",
          "lakeformation:GrantPermissions",
          "lakeformation:AddLFTagsToResource"
        ],
        "Resource" : "*"
      },
      {
        "Effect" : "Allow",
        "Action" : [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents",
        ],
        "Resource" : "*"
      }
    ]
  })

  tags = merge(
    {
      "Name" = format("%s-glue-job-policy", var.name)
    },
    var.tags
  )
}

resource "aws_iam_policy" "iam_glue_job_s3_policy" {
  name        = format("%s-glue-job-s3-policy", var.name)
  path        = "/"
  description = "This S3 policy used by the Glue Jobs"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect": "Allow",
        "Action": [
          "s3:PutObjectAcl",
          "s3:PutObject",
          "s3:GetObject",
          "s3:GetBucketAcl"
        ],
        "Resource": [
          "arn:aws:s3:::${var.raw_bucket_name}/*",
          "arn:aws:s3:::${var.insight_bucket_name}/*"
        ]
      }
    ]
  })

  tags = merge(
    {
      "Name" = format("%s-glue-job-s3-policy", var.name)
    },
    var.tags
  )
}