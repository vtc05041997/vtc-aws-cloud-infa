resource "aws_iam_policy" "iam_glue_crawler_policy" {
  name        = format("%s-glue-crawler-policy", var.name)
  path        = "/"
  description = "This role used by the Glue crawler"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "lakeformation:GetDataAccess",
          "lakeformation:GrantPermissions"
        ],
        "Resource" : "*"
      },
      {
        "Effect" : "Allow",
        "Action" : [
          "s3:GetBucketAcl",
          "s3:GetObject",
        ],
        "Resource" : [
          "arn:aws:s3:::${var.raw_bucket_name}/*",
          "arn:aws:s3:::${var.insight_bucket_name}/*"
        ]
      }
    ]
  })

  tags = merge(
    {
      "Name" = format("%s-glue-crawler-policy", var.name)
    },
    var.tags
  )
}

resource "aws_iam_role" "iam_glue_crawler_role" {
  name = format("%s-glue-crawler-role", var.name)

  assume_role_policy = jsonencode({
    "Version" : "2008-10-17",
    "Statement" : [
      {
        "Sid" : "",
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "glue.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
  })
  managed_policy_arns = [
    aws_iam_policy.iam_glue_crawler_policy.arn,
    # var.iam_readonly_kms_policy_arn,
    "arn:aws:iam::aws:policy/service-role/AWSGlueServiceRole"
  ]

  tags = merge(
    {
      "Name" = format("%s-glue-crawler-role", var.name)
    },
    var.tags
  )
}