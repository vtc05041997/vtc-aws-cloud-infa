locals {
  ec2_list = jsondecode(file("ec2_instances.json"))
  #   ec2_list = jsondecode(file("${path.root}/${var.ec2_file_name}"))
}

resource "aws_instance" "ec2_instance" {
  for_each               = { for server in local.ec2_list : server.application_name => server }
  ami                    = each.value.ami
  subnet_id              = var.map_subnet_name_id["${var.name}-${each.value.instance_subnet}"]
  instance_type          = each.value.instance_type
  key_name               = each.value.key_pair
  vpc_security_group_ids = [var.map_security_group_name_id["${var.name}-${each.value.security_group}-sg"]]

  tags = merge(
    {
      "Name" = format("%s-%s", var.name, each.value.application_name)
    },
    var.tags
  )
} 