variable "name" {
  type        = string
  description = "Name of the list ec2"
}

variable "tags" {
  type        = map(string)
  description = "Map of tags"
}

variable "vpc_id" {
  type = string
}

variable "map_subnet_name_id" {
  type = map(string)
}

variable "map_security_group_name_id" {
  type = map(string)
}
