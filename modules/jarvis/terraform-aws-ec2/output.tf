output "all_instances" {
  value       = aws_instance.ec2_instance
  description = "EC2 details"
}

output "map_name_id_instances_api" {
  value = { for s in aws_instance.ec2_instance : s.tags["Name"] => s.id if length(regexall("application", s.tags["Name"])) > 0 }
}

output "map_name_id_instances_bastion" {
  value = { for s in aws_instance.ec2_instance : s.tags["Name"] => s.id if length(regexall("bastion", s.tags["Name"])) > 0 }
}