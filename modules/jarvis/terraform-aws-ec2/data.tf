# data "aws_vpcs" "data_vpcs" {
#   tags = var.data_vpc_selection_tags
#   filter {
#     name   = "isDefault"
#     values = [false]
#   }
# }

# data "aws_vpc" "data_vpc" {
#   id = tolist(data.aws_vpcs.data_vpcs.ids)[0]
# }

data "aws_subnets" "all_subnets" {
  filter {
    name   = "vpc-id"
    values = [var.vpc_id]
  }
}

data "aws_subnet" "subnet_to_set" {
  for_each = toset(data.aws_subnets.all_subnets.ids)
  id       = each.value
}

output "map_subnet_name_id" {
  value = { for s in data.aws_subnet.subnet_to_set : s.tags["Name"] => s.id }
}

# map_subnet = {for s in data.aws_subnet.subnet_to_set : trimprefix(s.tags["Name"], "jarvis-congvt-public-subnet-b") => s.id }

# data "aws_subnets" "public_subnet_a_tags" {
#   filter {
#     name   = "vpc-id"
#     values = [data.aws_vpc.data_vpc.id]
#   }
#   tags = var.public_subnet_a_tags
# }

# data "aws_subnets" "public_subnet_b_tags" {
#   filter {
#     name   = "vpc-id"
#     values = [data.aws_vpc.data_vpc.id]
#   }
#   tags = var.public_subnet_b_tags
# }

# data "aws_subnets" "private_subnet_a_tags" {
#   filter {
#     name   = "vpc-id"
#     values = [data.aws_vpc.data_vpc.id]
#   }
#   tags = var.private_subnet_a_tags
# }

# data "aws_subnets" "private_subnet_b_tags" {
#   filter {
#     name   = "vpc-id"
#     values = [data.aws_vpc.data_vpc.id]
#   }
#   tags = var.private_subnet_b_tags
# }