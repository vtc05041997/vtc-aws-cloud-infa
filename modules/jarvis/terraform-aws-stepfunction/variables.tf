
variable "vtc_stormbreaker_api_asg_arn" {
  type = string
}

variable "iam_role_ecs_task_execution_arn" {
  type = string
}

variable "vtc_stormbreaker_api_ecr" {
  type = string
}

variable "vtc_stormbreaker_api_lb_tg" {
  type = string
}

variable "private_subnets" {
  type = set(string)
}

variable "public_subnets" {
  type = set(string)
}

variable "security_groups" {
  type = set(string)
}

variable "env" {
  type = string
}

variable "vtc_stormbreaker_api_cwl_gr_arn" {
  type = string
}

variable "vtc_stormbreaker_api_cwl_gr_name" {
  type = string
}