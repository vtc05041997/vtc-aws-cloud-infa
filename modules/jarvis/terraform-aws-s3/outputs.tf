output "raw_bucket_name" {
  value = aws_s3_bucket.raw.id
}

output "insight_bucket_name" {
  value = aws_s3_bucket.insight.id
}