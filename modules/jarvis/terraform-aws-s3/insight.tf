resource "aws_s3_bucket" "insight" {

  bucket = format("%s-%s-%s-%s-%s", var.master_prefix, var.environment, var.insight[0].bucket_name, data.aws_region.current.name, data.aws_caller_identity.current.account_id)
  acl    = "private"

  versioning {
    enabled = var.insight[0].versioning_enabled
  }

  lifecycle_rule {
    id      = "s3_lifecycle_config"
    prefix  = ""
    enabled = true

    dynamic "transition" {
      for_each = var.insight[0].current_ver_transitions
      content {
        days          = transition.value["days"]
        storage_class = transition.value["storage_class"]
      }
    }

    dynamic "noncurrent_version_transition" {
      for_each = var.insight[0].non_current_ver_transitions
      content {
        days          = noncurrent_version_transition.value["days"]
        storage_class = noncurrent_version_transition.value["storage_class"]
      }
    }

    dynamic "expiration" {
      for_each = var.insight[0].expiration
      content {
        days = expiration.value["days"]
      }
    }

    dynamic "noncurrent_version_expiration" {
      for_each = var.insight[0].non_current_ver_expiration
      content {
        days = noncurrent_version_expiration.value["days"]
      }
    }
  }
}

resource "aws_s3_bucket_intelligent_tiering_configuration" "insight" {
  bucket = aws_s3_bucket.insight.id
  name   = format("%s-%s-%s-%s", var.master_prefix, var.environment, var.insight[0].bucket_name, "all-objects")
  status = "Enabled"

  dynamic "tiering" {
    for_each = var.insight[0].intelligent_tiering
    content {
      days        = tiering.value["days"]
      access_tier = tiering.value["access_tier"]
    }
  }
}

// Block Public Access
resource "aws_s3_bucket_public_access_block" "insight" {

  bucket                  = format("%s-%s-%s-%s-%s", var.master_prefix, var.environment, var.insight[0].bucket_name, data.aws_region.current.name, data.aws_caller_identity.current.account_id)
  block_public_acls       = true
  block_public_policy     = true
  restrict_public_buckets = true
  ignore_public_acls      = true
  depends_on = [
    aws_s3_bucket.insight
  ]
}
