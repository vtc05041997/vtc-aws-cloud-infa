variable "master_prefix" {
  description = "Prefix for the S3 buckets"
  type        = string
}

variable "environment" {
  description = "Prefix for the S3 buckets"
  type        = string
}

variable "raw" {
  description = ""
  type        = list(any)
  default = [{
    # bucket_id          = "raw" //Do not change the bucket ID
    bucket_name = "raw"
    # s3_folders         = ["queue", "processed"]
    versioning_enabled = true
    current_ver_transitions = [
      {
        days          = 0
        storage_class = "INTELLIGENT_TIERING"
      }
    ]
    non_current_ver_transitions = []
    expiration                  = []
    non_current_ver_expiration  = []
    intelligent_tiering = [
      {
        days        = 180
        access_tier = "ARCHIVE_ACCESS"
      },
      {
        days        = 360
        access_tier = "DEEP_ARCHIVE_ACCESS"
      }
    ]
  }]
}

variable "insight" {
  description = ""
  type        = list(any)
  default = [{
    # bucket_id          = "raw" //Do not change the bucket ID
    bucket_name = "insight"
    # s3_folders         = ["queue", "processed"]
    versioning_enabled = true
    current_ver_transitions = [
      {
        days          = 0
        storage_class = "INTELLIGENT_TIERING"
      }
    ]
    non_current_ver_transitions = []
    expiration                  = []
    non_current_ver_expiration  = []
    intelligent_tiering = [
      {
        days        = 180
        access_tier = "ARCHIVE_ACCESS"
      },
      {
        days        = 350
        access_tier = "DEEP_ARCHIVE_ACCESS"
      }
    ]
  }]
}