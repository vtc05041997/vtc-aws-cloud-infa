variable "env" {
  type = string
}

variable "security_groups" {
  type = set(string)
}

variable "public_subnets" {
  type = set(string)
}

variable "private_subnets" {
  type = set(string)
}

variable "tags" {
  type = map(string)
}

variable "vpc_id" {
  type = string
}