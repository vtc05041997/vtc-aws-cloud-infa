resource "aws_lb" "vtc_stormbreaker_api_lb" {
  name               = format("vtc-stormbreaker-api-%s-lb", var.env)
  internal           = false
  load_balancer_type = "application"
  security_groups    = var.security_groups
  idle_timeout       = 60
  subnets            = var.public_subnets

  #enable_cross_zone_load_balancing = true alway ON alb no charge, off nlb and charge if on

  tags = merge(
    {
      "Name" = format("vtc-stormbreaker-api-%s-lb", var.env)
    },
    var.tags
  )
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.vtc_stormbreaker_api_lb.arn
  port              = "8080"
  protocol          = "HTTP"

  # ssl_policy        = "ELBSecurityPolicy-2016-08"
  # certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.vtc_stormbreaker_api_tg.arn
  }
}

resource "aws_lb_target_group" "vtc_stormbreaker_api_tg" {
  name     = format("vtc-stormbreaker-api-%s-tg", var.env)
  port     = 8080
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  target_type = "ip" # For ECS tasks you need to use IP to suiable mode "awsvpc" scale out
  
  health_check {
    # enabled             = true
    path                = "/healcheck"
    port                = "8080"
    # protocol            = "HTTP"
    # healthy_threshold   = 3
    # unhealthy_threshold = 2
    # interval            = 30
    # timeout             = 10
    matcher             = "200"
  }

  # depends_on = [aws_lb.alb]

  tags = merge(
    {
      "Name" = format("vtc-stormbreaker-api-%s-tg", var.env)
    },
    var.tags
  )
}

# resource "aws_lb_cookie_stickiness_policy" "this" {
#   name                     = format("%s-alb-sp", var.name)
#   load_balancer            = aws_lb.this.id
#   lb_port                  = 80
#   cookie_expiration_period = 600
# }

## For intances
# resource "aws_lb_target_group_attachment" "attachment" {
#   count            = length(var.instances)
#   target_group_arn = aws_lb_target_group.tg.arn
#   target_id        = var.map_name_id_instances_api[element(var.instances, count.index)]
#   port             = 80
# }


## For elb
# resource "aws_lb_target_group_attachment" "attachment" {
#   target_group_arn = aws_lb_target_group.tg.arn
#   target_id        = aws_autoscaling_group.my_asg.id
#   port             = 80

#   depends_on = [ aws_autoscaling_group.my_asg, aws_lb_target_group.tg]

# }

