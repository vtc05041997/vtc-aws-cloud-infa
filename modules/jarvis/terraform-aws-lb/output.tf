output "vtc_stormbreaker_api_lb" {
  value = aws_lb.vtc_stormbreaker_api_lb.id
}

output "vtc_stormbreaker_api_tg" {
  value = aws_lb_target_group.vtc_stormbreaker_api_tg.arn
}