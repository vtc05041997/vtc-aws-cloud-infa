resource "aws_cloudwatch_log_group" "vtc_stormbreaker_api_cwl_gr" {
  name = format("vtc-stormbreaker-api-%s-cwl-gr", var.env)

  tags = merge(
    {
      "Name" = format("vtc-stormbreaker-api-%s-cwl-gr", var.env)
    },
    var.tags
  )
}