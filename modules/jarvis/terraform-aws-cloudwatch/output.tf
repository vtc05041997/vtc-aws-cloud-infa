output "vtc_stormbreaker_api_cwl_gr_id" {
  value = aws_cloudwatch_log_group.vtc_stormbreaker_api_cwl_gr.id
}

output "vtc_stormbreaker_api_cwl_gr_name" {
  value = aws_cloudwatch_log_group.vtc_stormbreaker_api_cwl_gr.name
}

output "vtc_stormbreaker_api_cwl_gr_arn" {
  value = aws_cloudwatch_log_group.vtc_stormbreaker_api_cwl_gr.arn
}

output "vtc_glue_etl_cwl_gr_name" {
  value = aws_cloudwatch_log_group.vtc_glue_etl_cwl_gr.name
}

output "vtc_glue_etl_cwl_gr_arn" {
  value = aws_cloudwatch_log_group.vtc_glue_etl_cwl_gr.arn
}

output "vtc_glue_etl_cwl_gr_id" {
  value = aws_cloudwatch_log_group.vtc_glue_etl_cwl_gr.id
}