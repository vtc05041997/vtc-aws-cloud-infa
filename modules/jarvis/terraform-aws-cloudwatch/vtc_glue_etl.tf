resource "aws_cloudwatch_log_group" "vtc_glue_etl_cwl_gr" {
  name = format("vtc-glue-etl-%s-cwl-gr", var.env)

  tags = merge(
    {
      "Name" = format("vtc-glue-etl-%s-cwl-gr", var.env)
    },
    var.tags
  )
}