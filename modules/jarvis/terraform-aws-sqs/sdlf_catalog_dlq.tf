resource "aws_sqs_queue" "sqs_sdlf_catalog_dlq" {
  name                       = format("%s-%s-%s", var.master_prefix, var.env_prefix, "sdlf-catalog-dlq")
  message_retention_seconds  = 1209600
  visibility_timeout_seconds = 60
  tags = merge(
    {
      "Name" = format("%s-%s-%s", var.master_prefix, var.env_prefix, "sdlf-catalog-dlq")
    }
  )
}
