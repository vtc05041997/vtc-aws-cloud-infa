//
// Environment Prefix
//
variable "master_prefix" {
  description = "Prefix for the project"
  type        = string
}

variable "env_prefix" {
  description = "Prefix for the env"
  type        = string
}

variable "raw_bucket_name" {
  description = "Raw bucket name"
  type        = string
}

variable "golden_bucket_name" {
  description = "Golden bucket name"
  type        = string
}

variable "insight_bucket_name" {
  description = "Insight bucket name"
  type        = string
}

# //
# // Tags
# //
# variable "tags" {
#   description = "A map of tags to add to all resources"
#   type        = map(string)
# }

# # variable "lambda_tcb_landing_to_raw_arn" {
# #   description = "lambda arn"
# #   type        = string
# # }