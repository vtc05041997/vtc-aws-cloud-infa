resource "aws_sqs_queue" "sqs_sdlf_catalog_queue" {
  name = format("%s-%s-%s", var.master_prefix, var.env_prefix, "sdlf-catalog-queue")
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.sqs_sdlf_catalog_dlq.arn
    maxReceiveCount     = 10 # TEMP
  })
  visibility_timeout_seconds = 300 # TEMP
  tags = merge(
    {
      "Name" = format("%s-%s-%s", var.master_prefix, var.env_prefix, "sdlf-catalog-queue")
    }
  )
}

resource "aws_sqs_queue_policy" "sqs_queue_policy_s3" {
  queue_url = aws_sqs_queue.sqs_sdlf_catalog_queue.id
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "s3.amazonaws.com"
        },
        "Action" : "sqs:SendMessage",
        "Resource" : aws_sqs_queue.sqs_sdlf_catalog_queue.arn
        "Condition" : {
          "ArnEquals" : {
            "aws:SourceArn" : [
              "arn:aws:s3:::${var.raw_bucket_name}",
              "arn:aws:s3:::${var.golden_bucket_name}",
              "arn:aws:s3:::${var.insight_bucket_name}"
            ]
          },
          "StringEquals" : {
            "aws:SourceAccount" : "${data.aws_caller_identity.current.account_id}"
          }
        }
      }
    ]
  })
}
