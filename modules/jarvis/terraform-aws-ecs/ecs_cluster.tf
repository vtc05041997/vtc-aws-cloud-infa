resource "aws_ecs_cluster" "origin_cluster" {
  name = format("jarvis-%s-ecs-cluster", var.env)
}