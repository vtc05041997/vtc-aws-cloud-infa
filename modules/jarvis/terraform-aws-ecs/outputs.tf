output "ecs_cluster_name" {
    value = aws_ecs_cluster.origin_cluster.name
}

output "vtc_stormbreaker_api_ecs_service_name" {
    value = aws_ecs_service.vtc_stormbreaker_api_ecs_service.name
}