resource "aws_ecs_capacity_provider" "vtc_stormbreaker_api_capacity" {
  name = format("vtc-stormbreaker-api-%s-ecs-capacity", var.env)

  auto_scaling_group_provider {
    auto_scaling_group_arn = var.vtc_stormbreaker_api_asg_arn

    # If using auto scaling group then should'nt use manage scaling
    # managed_scaling {
    #   maximum_scaling_step_size = 100
    #   minimum_scaling_step_size = 1
    #   status                    = "ENABLED"
    #   target_capacity           = 3
    # }
  }
}

resource "aws_ecs_cluster_capacity_providers" "vtc_stormbreaker_api_providers" {
  cluster_name = aws_ecs_cluster.origin_cluster.name

  capacity_providers = [aws_ecs_capacity_provider.vtc_stormbreaker_api_capacity.name]

  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = aws_ecs_capacity_provider.vtc_stormbreaker_api_capacity.name
  }
}

resource "aws_ecs_task_definition" "vtc_stormbreaker_api_ecs_task" {
  family             = format("vtc-stormbreaker-api-%s-ecs-task", var.env)
  network_mode       = "awsvpc"
  execution_role_arn = var.iam_role_ecs_task_execution_arn
  task_role_arn      = var.iam_role_ecs_task_execution_arn
  memory             = 512
  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }

  container_definitions = jsonencode([
    {
      name      = format("vtc-stormbreaker-api-%s-docker-container", var.env)
      image     = format("%s:latest", var.vtc_stormbreaker_api_ecr)
      cpu       = 256
      memory    = 512
      essential = true

      portMappings = [
        {
          containerPort = 8080
          hostPort      = 8080
          protocol      = "tcp"
        }
      ],

      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-region        = data.aws_region.current.name
          awslogs-group         = var.vtc_stormbreaker_api_cwl_gr_name
          awslogs-stream-prefix = "ecs"
        }
      }

    }
  ])
}

resource "aws_ecs_service" "vtc_stormbreaker_api_ecs_service" {
  name            = format("vtc-stormbreaker-api-%s-ecs-service", var.env)
  cluster         = aws_ecs_cluster.origin_cluster.id
  task_definition = aws_ecs_task_definition.vtc_stormbreaker_api_ecs_task.arn
  desired_count   = 1

  network_configuration {
    subnets          = var.public_subnets
    security_groups  = var.security_groups
    # assign_public_ip = true
  }

  force_new_deployment = true
  placement_constraints {
    type = "distinctInstance"
  }

  triggers = {
    redeployment = timestamp()
  }

  capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.vtc_stormbreaker_api_capacity.name
    weight            = 100
  }

  load_balancer {
    target_group_arn = var.vtc_stormbreaker_api_lb_tg
    container_name   = format("vtc-stormbreaker-api-%s-docker-container", var.env)
    container_port   = 8080
  }
}