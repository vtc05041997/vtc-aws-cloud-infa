module "module_glue_crawlers_vtc_stormbreaker" {

  source = "./glue_crawler"
  master_prefix             = var.master_prefix
  database_prefix           = var.database_prefix
  tags                      = var.tags
  glue_crawler_filename     = "vtc_stormbreaker/glue_crawlers.json"
  iam_glue_crawler_role     = var.iam_glue_crawler_role
  namespace_to_s3bucketname = var.namespace_to_s3bucketname
}