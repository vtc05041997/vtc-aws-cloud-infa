locals {
  glue_crawler_lists = jsondecode(file("${path.root}/${var.glue_crawler_filename}"))

  # Default crawler to create a Single Schema for Each Amazon S3 Include Path
  # https://docs.aws.amazon.com/glue/latest/dg/crawler-configuration.html
  default_config = jsonencode(
    {
      Grouping = {
        TableGroupingPolicy = "CombineCompatibleSchemas"
      },
      Version = 1
    }
  )
}

resource "aws_glue_crawler" "this" {
  for_each      = {for v in local.glue_crawler_lists : "${v.namespace}-${v.table}" => v}
  database_name = "${var.database_prefix}-${each.value.namespace}"
  name          = format("%s-%s-%s-%s", var.master_prefix, "glue-cwl", each.value.namespace, each.value.table)
  role          = var.iam_glue_crawler_role

  s3_target {
    path = format("s3://%s/%s/%s", var.namespace_to_s3bucketname[each.value.namespace], each.value.namespace, each.value.table)
  }

  schema_change_policy {
    update_behavior = lookup(each.value, "update_behavior", "") != "" ? each.value.update_behavior : "LOG"
    delete_behavior = lookup(each.value, "delete_behavior", "") != "" ? each.value.delete_behavior : "DEPRECATE_IN_DATABASE"
  }

  configuration = lookup(
    each.value,
    "configuration",
    ""
  ) != "" ? jsonencode(each.value.configuration) : local.default_config

  tags = merge(
    var.tags,
    lookup(each.value, "extra_tags", {})
  )
}
