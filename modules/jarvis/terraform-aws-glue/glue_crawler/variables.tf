variable "master_prefix" {
  description = "Env prefix"
  type        = string
}

variable "database_prefix" {
  description = "Database prefix"
  type        = string
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
}

variable "iam_glue_crawler_role" {
  description = "role used by crawler"
  type        = string
}

variable "glue_crawler_filename" {
  description = "name of glue crawler json file"
  type        = string
}

variable "namespace_to_s3bucketname" {
  description = "Mapping namespace to S3 bucket name"
  type        = map(string)
}
