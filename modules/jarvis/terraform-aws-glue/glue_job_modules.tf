module "module_glue_jobs_vtc_stormbreaker" {
  source = "./glue_job"

  master_prefix             = var.master_prefix
  tags                      = var.tags
  iam_glue_job_role         = var.iam_glue_job_role
  glue_jobs_filename        = "vtc_stormbreaker/glue_jobs.json"
  raw_bucket_name           = var.raw_bucket_name
  glue_cloudwatch_log_group = var.vtc_stormbreaker_api_cwl_gr_name
#  glue_sec_config           = var.glue_sec_config_id

}