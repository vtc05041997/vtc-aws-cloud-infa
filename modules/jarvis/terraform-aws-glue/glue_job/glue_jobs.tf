locals {
  # glue_jobs_python = jsondecode(file("glue_jobs_python.json"))
  glue_jobs_lists = jsondecode(file("${path.root}/${var.glue_jobs_filename}"))
}

resource "aws_glue_job" "this" {
  for_each               = {for v in local.glue_jobs_lists : v.glue_job_name => v}
  name                   = format("%s-%s-%s", var.master_prefix, "glue-jb", each.value.glue_job_name)
  role_arn               = var.iam_glue_job_role
#  security_configuration = var.glue_sec_config
  glue_version           = each.value["type"] == "glueetl" ? each.value["glue_version"] : null

  execution_class   = each.value["type"] == "glueetl" ? (lookup(each.value, "execution_class", "") != "" ? each.value.execution_class : "FLEX")  : "STANDARD"
  number_of_workers = each.value["type"] == "glueetl" ? (lookup(each.value, "number_of_workers", "") != "" ? each.value.number_of_workers : 2) : null
  worker_type       = each.value["type"] == "glueetl" ? lookup(each.value, "worker_type", "G.1X") : null
  execution_property {
    max_concurrent_runs = each.value.max_concurrent_runs
  }
  command {
    name            = each.value.type
    script_location = format("s3://%s/etl_script/%s", var.raw_bucket_name, each.value.glue_script_filename)
    python_version  = lookup(each.value, "python_version", "") != "" ? each.value.python_version : null
  }
  connections       = lookup(each.value, "connections", "") != "" ? formatlist("${var.master_prefix}-%s", each.value.connections) : null
  default_arguments = {
    "--tempdir"                          = "s3://${var.raw_bucket_name}/glue_temporary/"
    "--continuous-log-loggroup"          = var.glue_cloudwatch_log_group
    "--enable-continuous-cloudwatch-log" = "true"
    "--enable-continuous-log-filter"     = "true"
    "--enable-metrics"                   = "true"
    "--job-language"                     = each.value.job_language
    "--class"                            = lookup(each.value, "scala_class", "") != "" ? each.value.scala_class : null
    "--job-bookmark-option"              = lookup(each.value, "enable_bookmark", false) ? "job-bookmark-enable" : "job-bookmark-disable"
    # "--extra-py-files"                   = lookup(local.glue_jobs_lists[count.index], "python_lib", "") != "" ? format("s3://%s/glue_libs/%s", var.artifacts_bucket_name, element(local.glue_jobs_lists.*.python_lib, count.index)) : null
    "--extra-py-files"                   = length(trimspace(each.value.python_lib)) > 0 ? join(",",formatlist("s3://%s/glue_libs/%s", var.raw_bucket_name, split(",", each.value.python_lib))) : null
    "--extra-jars"                       = length(trimspace(each.value.extra_jar)) > 0 ? join(",", formatlist("s3://%s/glue_libs/%s", var.raw_bucket_name, split(",", each.value.extra_jar))) : null
    "--additional-python-modules"        = lookup(each.value, "python_pip_lib", "") != "" ? join(",", formatlist("s3://%s/pip_pkg/%s", var.raw_bucket_name, split(",", each.value.python_pip_lib))) : null
    "--enable-glue-datacatalog"          = lookup(each.value, "enable_glue_data_catalog", false) ? true : null
    "--spark-event-logs-path"            = "s3://${var.raw_bucket_name}/glue-sparkui-logs/"

    # "--conf"                             = "spark.sql.catalog.glue_catalog=org.apache.iceberg.spark.sparkcatalog --conf spark.sql.catalog.glue_catalog.warehouse=s3://${var.golden_bucket_name}/golden_curated --conf spark.sql.catalog.glue_catalog.catalog-impl=org.apache.iceberg.aws.glue.gluecatalog --conf spark.sql.catalog.glue_catalog.io-impl=org.apache.iceberg.aws.s3.s3fileio --conf spark.sql.catalog.glue_catalog.glue.skip-name-validation=true --conf spark.sql.extensions=org.apache.iceberg.spark.extensions.icebergsparksessionextensions"
    # "--datalake-formats"                 = "iceberg"
    # "--extra-files"                      = length(trimspace(element(local.glue_jobs_python.*.other_lib, count.index))) > 0 ? format("s3://%s/glue_lib/%s", var.artifacts_bucket_name, element(local.glue_jobs_python.*.other_lib, count.index)) : "\"\""
    # "--insightbucket"                    = var.insight_bucket_name
    # "--goldenbucket"                     = var.golden_bucket_name
    # "--path"                             = format("s3://%s/queue/tcbtopic+0+0000000602.json", var.raw_bucket_name)
  }

  tags = merge(
    {
      "name" = format("%s-%s-%s", var.master_prefix, "glue-jb", each.value.glue_job_name)
    },
    var.tags,
    lookup(each.value, "extra_tags", {})
  )
}


# default_arguments = {
#     "--tempdir"                          = "s3://${var.temporary_bucket_name}/glue_temporary/"
#     "--continuous-log-loggroup"          = var.glue_cloudwatch_log_group
#     "--enable-continuous-cloudwatch-log" = "true"
#     "--enable-continuous-log-filter"     = "true"
#     "--enable-metrics"                   = "true"
#     "--job-language"                     = each.value.job_language
#     "--class"                            = lookup(each.value, "scala_class", "") != "" ? each.value.scala_class : null
#     "--job-bookmark-option"              = lookup(each.value, "enablebookmark", false) ? "job-bookmark-enable" : "job-bookmark-disable"
#     "--extra-py-files"                   = lookup(each.value, "python_lib", "") != "" ? format("s3://%s/glue_libs/%s", var.artifacts_bucket_name, each.value.python_lib) : null
#     "--extra-jars"                       = format(
#       "%s%s%s",
#       length(trimspace(each.value.extra_jar)) > 0 ? join(",", formatlist("s3://%s/glue_libs/%s", var.artifacts_bucket_name, split(",", each.value.extra_jar))) : "",
#       lookup(each.value, "disablelineage", false) == false && length(trimspace(each.value.extra_jar)) > 0 && try(var.spline_agent_version[lookup(each.value, "glue_version")], null) != null  ? "," : "",
#       lookup(each.value, "disablelineage", false) == false && try(var.spline_agent_version[lookup(each.value, "glue_version")], null) != null ? format("s3://%s/glue_libs/%s", var.artifacts_bucket_name, var.spline_agent_version[lookup(each.value, "glue_version")]) : ""
#     ) != "" ? format(
#       "%s%s%s",
#       length(trimspace(each.value.extra_jar)) > 0 ? join(",", formatlist("s3://%s/glue_libs/%s", var.artifacts_bucket_name, split(",", each.value.extra_jar))) : "",
#       lookup(each.value, "disablelineage", false) == false && length(trimspace(each.value.extra_jar)) > 0 && try(var.spline_agent_version[lookup(each.value, "glue_version")], null) != null ? "," : "",
#       lookup(each.value, "disablelineage", false) == false && try(var.spline_agent_version[lookup(each.value, "glue_version")], null) != null ? format("s3://%s/glue_libs/%s", var.artifacts_bucket_name, var.spline_agent_version[lookup(each.value, "glue_version")]) : ""
#     ) : null
#     "--additional-python-modules"        = lookup(each.value, "python_pip_lib", "") != "" ? join(",", formatlist("s3://%s/pip_pkg/%s", var.artifacts_bucket_name, split(",", each.value.python_pip_lib))) : null
#     "--enable-glue-datacatalog"          = lookup(each.value, "enablegluedatacatalog", false) ? true : null
#     "--spark-event-logs-path"            = "s3://${var.temporary_bucket_name}/glue-sparkui-logs/"
#     "--write-shuffle-files-to-s3"        = lookup(each.value, "write_shuffle_files_to_s3", false) ? each.value.write_shuffle_files_to_s3 : null
#     "--write-shuffle-spills-to-s3"       = lookup(each.value, "write_shuffle_spills_to_s3", false) ? each.value.write_shuffle_spills_to_s3 : null
#     "--conf"                             = format(
#       "%s%s%s%s%s",
#       lookup(each.value, "extra_conf", "") != "" ? each.value.extra_conf : "",
#       lookup(each.value, "extra_conf", "") != "" && lookup(each.value, "write_shuffle_files_to_s3", false) ? " --conf " : "",
#       lookup(each.value, "write_shuffle_files_to_s3", false) ? "spark.shuffle.glue.s3shufflebucket=s3://${var.temporary_bucket_name}/glue_temporary" : "",
#       lookup(each.value, "disablelineage", false) == false && (lookup(each.value, "extra_conf", "") != "" || lookup(each.value, "write_shuffle_files_to_s3", false) == true) && try(var.spline_agent_version[lookup(each.value, "glue_version")], null) != null  ? " --conf " : "",
#       lookup(each.value, "disablelineage", false) == false && try(var.spline_agent_version[lookup(each.value, "glue_version")], null) != null ? replace(var.spline_conf, "{{ var.env_prefix }}", var.env_prefix) : ""
#     ) != "" ? format(
#       "%s%s%s%s%s",
#       lookup(each.value, "extra_conf", "") != "" ? each.value.extra_conf : "",
#       lookup(each.value, "extra_conf", "") != "" && lookup(each.value, "write_shuffle_files_to_s3", false) ? " --conf " : "",
#       lookup(each.value, "write_shuffle_files_to_s3", false) ? "spark.shuffle.glue.s3shufflebucket=s3://${var.temporary_bucket_name}/glue_temporary" : "",
#       lookup(each.value, "disablelineage", false) == false && (lookup(each.value, "extra_conf", "") != "" || lookup(each.value, "write_shuffle_files_to_s3", false) == true) && try(var.spline_agent_version[lookup(each.value, "glue_version")], null) != null  ? " --conf " : "",
#       lookup(each.value, "disablelineage", false) == false && try(var.spline_agent_version[lookup(each.value, "glue_version")], null) != null ? replace(var.spline_conf, "{{ var.env_prefix }}", var.env_prefix) : ""
#     ) : null
#   }