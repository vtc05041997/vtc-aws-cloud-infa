variable "master_prefix" {
  description = "Prefix for the S3 buckets"
  type        = string
}


variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
}

variable "iam_glue_job_role" {
  description = "role used by glue jobs"
  type        = string
}


variable "glue_jobs_filename" {
  description = "name of glue job json file"
  type        = string
}


variable "glue_cloudwatch_log_group" {
  description = "cloud watch log group for glue"
  type        = string
}

#variable "glue_sec_config" {
#  description = "glue security configuration"
#  type        = string
#}

variable "spline_agent_version" {
  type = map(string)
  default = {
    "2.0" = "spark-2.4-spline-agent-bundle-2.11-1.1.0.jar"
    "3.0" = "spark-3.1-spline-agent-bundle-2.12-1.1.0.jar"
    "4.0" = "spark-3.3-spline-agent-bundle-2.12-1.1.0.jar"
  }
}

variable "spline_conf" {
  type = string
  default = "spark.sql.queryExecutionListeners=za.co.absa.spline.harvester.listener.SplineQueryExecutionListener --conf spline.lineageDispatcher=s3 --conf spline.lineageDispatcher.s3.className=za.co.absa.spline.harvester.dispatcher.S3LineageDispatcher --conf spline.lineageDispatcher.s3.apiVersion=1.2 --conf spline.lineageDispatcher.s3.bucket=tcb-data-{{ var.env_prefix }}-apse-1-data-management --conf spline.lineageDispatcher.s3.bucketPrefix=raw-data/metadata-management/lineage/raw"
}

variable "raw_bucket_name" {
  type = string
}
