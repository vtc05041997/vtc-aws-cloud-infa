variable "master_prefix" {
  description = "Prefix for the S3 buckets"
  type        = string
}

variable "database_prefix" {
  description = "Database prefix"
  type        = string
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
}

variable "iam_glue_job_role" {
  description = "Glue job IAM Role ARN"
  type        = string
}

variable "iam_glue_crawler_role" {
  description = "Glue job IAM Role ARN"
  type        = string
}

variable "vtc_stormbreaker_api_cwl_gr_name" {
  description = "Cloudwatch log group name"
  type        = string
}

#variable "glue_sec_config_id" {
#  description = "Glue security config id"
#  type        = string
#}

#variable "iam_readonly_kms_policy_arn" {
#  description = "IAM readonly KMS policy ARN"
#  type        = string
#}

variable "raw_bucket_name" {
  description = "Artifacts bucket name"
  type        = string
}

variable "namespace_to_s3bucketname" {
  description = "Mapping namespace to S3 bucket name"
  type        = map(string)
}