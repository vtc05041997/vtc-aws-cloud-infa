resource "aws_ecr_repository" "vtc_stormbreaker_api_ecr" {
  name                 = format("vtc-stormbreaker-api-%s-ecr", var.env)
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }

  tags = merge(var.tags)
}
