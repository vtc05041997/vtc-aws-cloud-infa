variable "name" {
  description = "Name of vpc"
  type        = string
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
}

variable "data_vpc_default_security_group_name" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "public_route_table_id" {
  type = set(string)
}

variable "public_subnet_id" {
  type = set(string)
}

# variable "security_group_ids" {
#   type = set(string)
# }