# # ################################################################################
# # # Interface Endpoint
# # ################################################################################
# locals {
#   endpoints_list_interface = jsondecode(file("interface_endpoint.json"))
#   #   ec2_list = jsondecode(file("${path.root}/${var.ec2_file_name}"))
# }

# resource "aws_vpc_endpoint" "autoscaling" {
#   for_each          = { for endpoint in local.endpoints_list_interface : endpoint.service_name => endpoint }
#   vpc_id            = var.vpc_id
#   service_name      = "com.amazonaws.${data.aws_region.current.name}.${each.value.service_name}"
#   vpc_endpoint_type = "Interface"

#   subnet_ids = [data.aws_subnet_ids.private_subnets.id]

#   security_group_ids = [
#     data.aws_security_group.default.id
#   ]

#   private_dns_enabled = true

#   tags = merge(
#     var.tags,
#     {
#       "Name" = "${var.name}-vce-${each.value.service_name}"
#     },
#   )
# }
