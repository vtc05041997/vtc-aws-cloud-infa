output "vtc_stormbreaker_api_asg_id" {
  value = aws_autoscaling_group.vtc_stormbreaker_api_asg.id
}

output "vtc_stormbreaker_api_asg_arn" {
  value = aws_autoscaling_group.vtc_stormbreaker_api_asg.arn
}