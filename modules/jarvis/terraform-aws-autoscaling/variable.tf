variable "env" {
  type = string
}

variable "public_subnets" {
  type = set(string)
}

variable "private_subnets" {
  type = set(string)
}

variable "vtc_stormbreaker_api_template_id" {
  type = string
}