resource "aws_autoscaling_group" "vtc_stormbreaker_api_asg" {
  name_prefix         = format("vtc-stormbreaker-api-%s-asg", var.env)
  desired_capacity    = 1
  max_size            = 2
  min_size            = 1
  vpc_zone_identifier = var.public_subnets

  health_check_type         = "EC2"
  health_check_grace_period = 300 # default is 300 seconds  
  # Launch Template
  launch_template {
    id      = var.vtc_stormbreaker_api_template_id
    version = "$Latest"
  }
  # Instance Refresh
  instance_refresh {
    strategy = "Rolling"
    preferences {
      instance_warmup        = 300 # Default behavior is to use the Auto Scaling Group's health check grace period.
      min_healthy_percentage = 50
    }
    triggers = [/*"launch_template",*/ "desired_capacity"] # You can add any argument from ASG here, if those has changes, ASG Instance Refresh will trigger
  }
  tag {
    key                 = "Application"
    value               = "Api"
    propagate_at_launch = true
  }
}

# resource "aws_autoscaling_attachment" "this" {
#   autoscaling_group_name = aws_autoscaling_group.vtc_stormbreaker_api_asg.id
#   lb_target_group_arn    = var.vtc_stormbreaker_api_tg
# }

resource "aws_cloudwatch_metric_alarm" "vtc_stormbreaker_api_cwa_su" {
  alarm_name          = format("vtc-stormbreaker-api-%s-cwa-su", var.env)
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 2
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = 120
  statistic           = "Average"
  threshold           = 80

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.vtc_stormbreaker_api_asg.name
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.vtc_stormbreaker_api_asg_po_su.arn]
}

resource "aws_autoscaling_policy" "vtc_stormbreaker_api_asg_po_su" {
  name                   = format("vtc-stormbreaker-api-%s-policy-scale-up", var.env)
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.vtc_stormbreaker_api_asg.name
}

resource "aws_cloudwatch_metric_alarm" "vtc_stormbreaker_api_cwa_sd" {
  alarm_name          = format("vtc-stormbreaker-api-%s-cwa-sd", var.env)
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 2
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = 120
  statistic           = "Average"
  threshold           = 30

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.vtc_stormbreaker_api_asg.name
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.vtc_stormbreaker_api_asg_po_sd.arn]
}

resource "aws_autoscaling_policy" "vtc_stormbreaker_api_asg_po_sd" {
  name                   = format("vtc-stormbreaker-api-%s-policy-scale-down", var.env)
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.vtc_stormbreaker_api_asg.name
}