# ################################################################################
# # NAT Gateway One public subnet => one nat gateway
# ################################################################################

# resource "aws_eip" "ip_nat" {
#   vpc = true
#   count = length(aws_subnet.public_subnets.*.id)

#   tags = merge(
#     {
#       "Name" = format("%s-eip-%s", var.name, count.index + 1)
#     },
#     var.tags
#   )
#   depends_on = [ aws_vpc.vpc ]
# }

# resource "aws_nat_gateway" "ngw" {

#   count = length(aws_subnet.public_subnets.*.id)

#   allocation_id = element(aws_eip.ip_nat.*.id, count.index)
#   subnet_id     = element(aws_subnet.public_subnets.*.id, count.index)

#   tags = merge(
#     {
#       "Name" = format("%s-ngw-%s", var.name, count.index + 1)
#     },
#     var.tags
#   )

#   # To ensure proper ordering, it is recommended to add an explicit dependency
#   # on the Internet Gateway for the VPC.
#   depends_on = [aws_internet_gateway.igw]
# }

# resource "aws_route" "private_nat_gateway" {
#   count = length(aws_nat_gateway.ngw.*.id)

#   route_table_id         = element(aws_route_table.private_route_table.*.id, count.index)
#   destination_cidr_block = element(var.private_subnet_cidrs, count.index)
# #   destination_cidr_block = "10.0.32.0/16"
#   nat_gateway_id         = element(aws_nat_gateway.ngw.*.id, count.index)

#   timeouts {
#     create = "5m"
#   }

#   depends_on = [ aws_route_table.private_route_table ]
# }