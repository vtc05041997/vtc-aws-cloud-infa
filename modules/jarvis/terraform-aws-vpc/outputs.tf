output "public_subnets" {
  description = "List of IDs of public subnets"
  value       = aws_subnet.public_subnets.*.id
}

output "public_subnet_arns" {
  description = "List of ARNs of public subnets"
  value       = aws_subnet.public_subnets.*.arn
}

output "public_subnets_cidr_blocks" {
  description = "List of cidr_blocks of public subnets"
  value       = aws_subnet.public_subnets.*.cidr_block
}

output "private_subnets" {
  description = "List of IDs of public subnets"
  value       = aws_subnet.private_subnets.*.id
}

output "private_subnet_arns" {
  description = "List of ARNs of public subnets"
  value       = aws_subnet.private_subnets.*.arn
}

output "private_subnets_cidr_blocks" {
  description = "List of cidr_blocks of public subnets"
  value       = aws_subnet.private_subnets.*.cidr_block
}

output "vpc" {
  description = "VPC id"
  value       = aws_vpc.vpc.id
}

data "aws_subnets" "all_subnets" {
  filter {
    name   = "vpc-id"
    values = [aws_vpc.vpc.id]
  }
}

data "aws_subnet" "subnet_to_set" {
  for_each = toset(data.aws_subnets.all_subnets.ids)
  id       = each.value
}

output "map_subnet_name_id" {
  value = { for s in data.aws_subnet.subnet_to_set : s.tags["Name"] => s.id }
}

# output "nat_gateway_ip" {
#   value = aws_eip.ip_nat.*.id
# }

# output "aws_nat_gateway_id" {
#   value = aws_nat_gateway.ngw.*.id
# }

# output "aws_vpc_peering_connection_id" {
#   value       = aws_vpc_peering_connection.peering_connection.id
#   description = "ID connection to VPC peering"
# }

output "public_route_table_id" {
  value = aws_route_table.public_route_table.*.id
}

output "private_route_table_id" {
  value = aws_route_table.private_route_table.*.id
}