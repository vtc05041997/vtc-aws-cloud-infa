variable "name" {
  description = "Name of vpc"
  type        = string
}

variable "cidr_block" {
  description = "CIDR block for vpc"
  type        = string
}

variable "public_subnet_cidrs" {
  description = "List cidrs of public subnets"
  type        = list(string)
}

variable "public_subnet_name" {
  description = "List name of public subnets"
  type        = list(string)
}

variable "public_subnet_az" {
  description = "List az of public subnets"
  type        = list(string)
}

variable "private_subnet_cidrs" {
  description = "List cidrs of public subnets"
  type        = list(string)
}

variable "private_subnet_name" {
  description = "List name of public subnets"
  type        = list(string)
}

variable "private_subnet_az" {
  description = "List az of public subnets"
  type        = list(string)
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
}

# variable "peer_owner_id" {
#   description = "Owner id of VPC"
#   type        = string
# }

# variable "current_vpc" {
#   description = "Current VPC"
#   type        = string
# }
# variable "target_vpc" {
#   description = "Target id of VPC"
#   type        = string
# }

variable "map_public_ip_on_launch" {
  type        = bool
  default     = true
  description = "Should be false if you do not want to auto-assign public IP on launch"
}

variable "enable_dns_hostnames" {
  description = "Should be true to enable DNS hostnames in the VPC"
  type        = bool
  default     = false
}

variable "enable_dns_support" {
  description = "Should be true to enable DNS support in the VPC"
  type        = bool
  default     = true
}