output "vtc_stormbreaker_api_template_name" {
  value = aws_launch_template.vtc_stormbreaker_api_template.name
}

output "vtc_stormbreaker_api_template_id" {
  value = aws_launch_template.vtc_stormbreaker_api_template.id
}