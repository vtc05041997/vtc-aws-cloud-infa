locals {
  var_env = read_terragrunt_config(find_in_parent_folders("var_env.hcl"))
  project = read_terragrunt_config(find_in_parent_folders("project.hcl"))

  # Extract out common variables for reuse
  account_name          = local.var_env.locals.account_name
  aws_account_id        = local.var_env.locals.aws_account_id
  aws_profile           = local.var_env.locals.aws_profile
  aws_iam_sso_role_name = local.var_env.locals.aws_iam_sso_role_name
  environment           = local.var_env.locals.environment
  environment_code      = local.var_env.locals.environment_code
  aws_region            = local.var_env.locals.aws_region
  aws_region_code       = local.var_env.locals.aws_region_code
  database_prefix       = local.var_env.locals.database_prefix
  master_prefix         = local.var_env.locals.master_prefix


  project_name = local.project.locals.project_name
  application  = local.project.locals.application
  branch_name  = local.project.locals.branch_name

}

dependency s3 {
  config_path = "../../../jarvis/s3"
}

dependency iam {
  config_path = "../../../jarvis/iam"
}


terraform {
  source = "../../../../../modules/projects/vtc-orchs/terraform-aws-codepipelines"
}

# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {

  raw_bucket_name       = dependency.s3.outputs.raw_bucket_name
  name                  = "${local.project_name}-${local.application}"
  codebuild_role_arn    = dependency.iam.outputs.codebuild_role_arn
  codepipeline_role_arn = dependency.iam.outputs.codepipeline_role_arn
  environment           = local.environment

  // code commit
  branch_name = local.branch_name

  // codebuild docker image general
  codebuild_image                       = "aws/codebuild/amazonlinux2-x86_64-standard:3.0"
  codebuild_container_type              = "LINUX_CONTAINER"
  codebuild_image_pull_credentials_type = "CODEBUILD"

  // 3GB, 2vCPUs
  codebuild_general1_small_compute_type = "BUILD_GENERAL1_SMALL"

  // codebuild source
  codebuild_type = "CODEPIPELINE"
  buildspec      = "buildspec.yaml"

  // s3 buckets
  // artifacts_bucket_name       = "${local.master_prefix}-${local.environment}-artifacts-${local.aws_region}-${local.aws_account_id}"

  tags = {
    "da-terraform" : true
    "name" : local.project_name
    "application" : local.application
    "branch_name" : local.branch_name
  }
}
