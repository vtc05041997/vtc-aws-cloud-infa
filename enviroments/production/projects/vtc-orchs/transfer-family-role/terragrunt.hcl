locals {
  account = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  region  = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  env     = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  project = read_terragrunt_config(find_in_parent_folders("project.hcl"))
  common  = yamldecode(file(find_in_parent_folders("common_vars.yaml")))

  # Extract out common variables for reuse
  aws_account_id        = local.account.locals.aws_account_id
  aws_region            = local.region.locals.aws_region
  aws_region_code       = local.region.locals.aws_region_code
  environment           = local.env.locals.environment
  env_code              = local.env.locals.environment-code
  branch_name           = local.env.locals.branch_name
  aws_iam_sso_role_name = local.account.locals.aws_iam_sso_role_name

  tcb_prefix    = local.common.tcb_prefix
  master_prefix = local.common.master_prefix
  vpc_env_name  = local.env.locals.vpc_env_name

  project_code               = local.project.locals.project_code
  name                       = local.project.locals.name
  app_name                   = local.project.locals.app_name
  project_name               = local.project.locals.project_name
  project_id                 = local.project.locals.project_id
  application                = local.project.locals.application
  application_id             = local.project.locals.application_id
  business_unit              = local.project.locals.business_unit
  cost_center                = local.project.locals.cost_center
  cost_pool                  = local.project.locals.cost_pool
  pl_code                    = local.project.locals.pl_code
  development_team           = local.project.locals.development_team
  support_team               = local.project.locals.support_team
  data_classification        = local.project.locals.data_classification
  data_zone                  = local.project.locals.data_zone
  data_layer                 = local.project.locals.data_layer
  data_movement              = local.project.locals.data_movement
  system_tier_classification = local.project.locals.system_tier_classification
  compliance_framework       = local.project.locals.compliance_framework
  uptime                     = local.project.locals.uptime

}

# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  source = "../../../../../modules/new-convention/delivery-corp-twbs1-encrypt/terraform-aws-iam"
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

# Access necessary module’s output variables
dependency "kms" {
  config_path = "../../kms"
}

# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {

  name_prefix          = "${local.development_team}-${local.project_code}-${local.project_name}-${local.environment}-${local.aws_region_code}"
  env_prefix           = local.environment
  master_prefix        = local.master_prefix
  serving_layer_bucket = "serving-layer-${local.env_code}-${local.aws_region}-s3"

  tags = {
    "da-terraform" : true
    "name" : local.name
    "application" : local.application
    "application_id" : local.application_id
    "environment" : local.environment
    "project-id" : local.project_id
    "business-unit" : local.business_unit
    "cost-center" : local.cost_center
    "cost-pool" : local.cost_pool
    "pl-code" : local.pl_code
    "support-team" : local.support_team
    "development-team" : local.development_team
    "data-classification" : local.data_classification
    "data-zone" : local.data_zone
    "data-layer" : local.data_layer
    "data-movement" : local.data_movement
    "system-tier-classification" : local.system_tier_classification
    "compliance-framework" : local.compliance_framework
    "uptime" : local.uptime
  }
}
