locals {
  var_env = read_terragrunt_config(find_in_parent_folders("var_env.hcl"))

  # Extract out common variables for reuse
  account_name          = local.var_env.locals.account_name
  aws_account_id        = local.var_env.locals.aws_account_id
  aws_profile           = local.var_env.locals.aws_profile
  aws_iam_sso_role_name = local.var_env.locals.aws_iam_sso_role_name
  environment           = local.var_env.locals.environment
  environment_code      = local.var_env.locals.environment_code
  aws_region            = local.var_env.locals.aws_region
  aws_region_code       = local.var_env.locals.aws_region_code
  database_prefix       = local.var_env.locals.database_prefix
  master_prefix         = local.var_env.locals.master_prefix
  infa                  = "vpc"

}

# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  source = "../../../../modules/jarvis//terraform-aws-vpc"
}

inputs = {
  master_prefix = local.master_prefix
  environment   = local.environment

  // sqs_sdlf_catalog_queue_arn = "arn:aws:sqs:${local.aws_region}:${local.aws_account_id}:${local.common_vars.master_prefix}-${local.env}-sdlf-catalog-queue"
  // sqs_sdlf_notify_for_glue_queue_arn = "arn:aws:sqs:${local.aws_region}:${local.aws_account_id}:${local.common_vars.master_prefix}-${local.env}-sdlf-notify-for-glue-queue"

  tags = {
    "Terraform" : true
    "tcb:environment-name" : local.environment
  }
}