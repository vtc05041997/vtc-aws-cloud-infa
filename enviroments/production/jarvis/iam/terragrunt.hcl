locals {
  var_env = read_terragrunt_config(find_in_parent_folders("var_env.hcl"))

  # Extract out common variables for reuse
  account_name          = local.var_env.locals.account_name
  aws_account_id        = local.var_env.locals.aws_account_id
  aws_profile           = local.var_env.locals.aws_profile
  aws_iam_sso_role_name = local.var_env.locals.aws_iam_sso_role_name
  environment           = local.var_env.locals.environment
  environment_code      = local.var_env.locals.environment_code
  aws_region            = local.var_env.locals.aws_region
  aws_region_code       = local.var_env.locals.aws_region_code
  database_prefix       = local.var_env.locals.database_prefix
  master_prefix         = local.var_env.locals.master_prefix

}

# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  source = "../../../../modules/jarvis//terraform-aws-iam"
}

dependency s3 {
  config_path = "../s3"
}

// dependency "kms" {
//   config_path = "../kms"
// }

# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {

  name            = "${local.master_prefix}-${local.environment}"
  raw_bucket_name = dependency.s3.outputs.raw_bucket_name

  tags = {
    "Terraform" : true
    "tcb:environment-name" : local.environment
  }
}
