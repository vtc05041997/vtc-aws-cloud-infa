# Set account-wide variables. These are automatically pulled in to configure the remote state bucket in the root
# terragrunt.hcl configuration.

locals {
  account_name          = "congvt"
  aws_account_id        = "966302881271"
  aws_profile           = "terraform-congvt"
  aws_iam_sso_role_name = "AWSReservedSSO_AdministratorAccess_362f36534f821acds"
  environment           = "congvt"
  environment_code      = "congvt"

  aws_region      = "ap-southeast-1"
  aws_region_code = "apse-1"

  master_prefix   = "jarvis"
  database_prefix = "jarvis"

}
