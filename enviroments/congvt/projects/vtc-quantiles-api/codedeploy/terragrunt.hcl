locals {
  var_env = read_terragrunt_config(find_in_parent_folders("var_env.hcl"))
  project = read_terragrunt_config(find_in_parent_folders("project.hcl"))

  # Extract out common variables for reuse
  account_name          = local.var_env.locals.account_name
  aws_account_id        = local.var_env.locals.aws_account_id
  aws_profile           = local.var_env.locals.aws_profile
  aws_iam_sso_role_name = local.var_env.locals.aws_iam_sso_role_name
  environment           = local.var_env.locals.environment
  environment_code      = local.var_env.locals.environment_code
  aws_region            = local.var_env.locals.aws_region
  aws_region_code       = local.var_env.locals.aws_region_code
  database_prefix       = local.var_env.locals.database_prefix
  master_prefix         = local.var_env.locals.master_prefix


  project_name = local.project.locals.project_name
  application  = local.project.locals.application
  branch_name  = local.project.locals.branch_name

}

#To inherit this configuration, in each of the child terragrunt.hcl files, 
#You can tell Terragrunt to automatically include all the settings from the root terragrunt.hcl file as follows:
#Flow the doccument: https://terragrunt.gruntwork.io/docs/features/keep-your-remote-state-configuration-dry/
include "root" {
  path = find_in_parent_folders()
}

dependency deployment {
  config_path = "../deployment"
}

dependency iam {
  config_path = "../../../jarvis/iam"
}

terraform {
  source = "../../../../../modules/projects/vtc-quantiles-api//terraform-aws-codedeploy"
}


# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {

  name                = "${local.project_name}-${local.application}"
  lb_target_group     = dependency.deployment.outputs.lb_target_group
  auto_scaling_groups = [dependency.deployment.outputs.auto_scaling_groups]
  codedeploy_role_arn = dependency.iam.outputs.codedeploy_role_arn
  application_type    = "api"

  tags = {
    "da-terraform" : true
    "name" : local.project_name
    "application" : local.application
    "branch_name" : local.branch_name
  }
}
