locals {
  var_env = read_terragrunt_config(find_in_parent_folders("var_env.hcl"))
  project = read_terragrunt_config(find_in_parent_folders("project.hcl"))

  # Extract out common variables for reuse
  account_name          = local.var_env.locals.account_name
  aws_account_id        = local.var_env.locals.aws_account_id
  aws_profile           = local.var_env.locals.aws_profile
  aws_iam_sso_role_name = local.var_env.locals.aws_iam_sso_role_name
  environment           = local.var_env.locals.environment
  environment_code      = local.var_env.locals.environment_code
  aws_region            = local.var_env.locals.aws_region
  aws_region_code       = local.var_env.locals.aws_region_code
  database_prefix       = local.var_env.locals.database_prefix
  master_prefix         = local.var_env.locals.master_prefix


  project_name = local.project.locals.project_name
  application  = local.project.locals.application
  branch_name  = local.project.locals.branch_name

}

#To inherit this configuration, in each of the child terragrunt.hcl files, 
#You can tell Terragrunt to automatically include all the settings from the root terragrunt.hcl file as follows:
#Flow the doccument: https://terragrunt.gruntwork.io/docs/features/keep-your-remote-state-configuration-dry/
include "root" {
  path = find_in_parent_folders()
}

dependency vpc {
  config_path = "../../../jarvis/vpc"
}

dependency iam {
  config_path = "../../../jarvis/iam"
}

dependency sg {
  config_path = "../../../jarvis/sg"
}

terraform {
  source = "../../../../../modules/projects/vtc-quantiles-api//terraform-aws-deployment"
}


# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {

  name             = "${local.project_name}-${local.application}"
  vpc_id           = dependency.vpc.outputs.vpc
  security_groups  = [dependency.sg.outputs.security_group_api_id]
  public_subnets   = dependency.vpc.outputs.public_subnets
  private_subnets  = dependency.vpc.outputs.private_subnets
  instance_keypair = "key-for-ec2"
  instance_type    = "t2.micro"
  image_id         = "ami-06018068a18569ff2"
  instances        = ["application-server-1", "application-server-2"]
  application_type = "api"

  aws_iam_instance_profile_arn = dependency.iam.outputs.aws_iam_instance_profile_arn

  tags = {
    "da-terraform" : true
    "name" : local.project_name
    "application" : local.application
    "branch_name" : local.branch_name
  }
}
