locals {
  var_env = read_terragrunt_config(find_in_parent_folders("var_env.hcl"))

  # Extract out common variables for reuse
  account_name          = local.var_env.locals.account_name
  aws_account_id        = local.var_env.locals.aws_account_id
  aws_profile           = local.var_env.locals.aws_profile
  aws_iam_sso_role_name = local.var_env.locals.aws_iam_sso_role_name
  environment           = local.var_env.locals.environment
  environment_code      = local.var_env.locals.environment_code
  aws_region            = local.var_env.locals.aws_region
  aws_region_code       = local.var_env.locals.aws_region_code
  database_prefix       = local.var_env.locals.database_prefix
  master_prefix         = local.var_env.locals.master_prefix
  infa                  = "glue"
}

# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  source = "../../../../modules/jarvis//terraform-aws-glue"
}

#To inherit this configuration, in each of the child terragrunt.hcl files, 
#You can tell Terragrunt to automatically include all the settings from the root terragrunt.hcl file as follows:
#Flow the doccument: https://terragrunt.gruntwork.io/docs/features/keep-your-remote-state-configuration-dry/
include "root" {
  path = find_in_parent_folders()
}


dependency s3 {
   config_path = "../s3"
}

dependency iam {
  config_path = "../iam"
}

dependency clw {
  config_path = "../cloud-watch"
}

# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {

  master_prefix   = "${local.master_prefix}-${local.environment}"
  database_prefix = local.database_prefix
  raw_bucket_name = dependency.s3.outputs.raw_bucket_name

  iam_glue_job_role                = dependency.iam.outputs.iam_glue_job_role_arn
  iam_glue_crawler_role            = dependency.iam.outputs.iam_glue_crawler_role_arn
  vtc_stormbreaker_api_cwl_gr_name = dependency.clw.outputs.vtc_glue_etl_cwl_gr_name

  namespace_to_s3bucketname = {
    "jarvis_raw"    : "jarvis-${local.environment}-raw-${local.aws_region}-${local.aws_account_id}"
    "jarvis_insight": "jarvis-${local.environment}-insight-${local.aws_region}-${local.aws_account_id}"
  }

  tags = {
    "Terraform" : true
    "vtc:environment-name" : local.environment
    "vtc:aws-region" : local.aws_region
    "vtc:infa" : local.infa
  }
}
