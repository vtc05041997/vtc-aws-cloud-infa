locals {
  # Automatically load environment-level variables
  var_env = read_terragrunt_config(find_in_parent_folders("var_env.hcl"))

  # Extract out common variables for reuse
  account_name          = local.var_env.locals.account_name
  aws_account_id        = local.var_env.locals.aws_account_id
  aws_profile           = local.var_env.locals.aws_profile
  aws_iam_sso_role_name = local.var_env.locals.aws_iam_sso_role_name
  environment           = local.var_env.locals.environment
  environment_code      = local.var_env.locals.environment_code
  aws_region            = local.var_env.locals.aws_region
  aws_region_code       = local.var_env.locals.aws_region_code
  database_prefix       = local.var_env.locals.database_prefix
  master_prefix         = local.var_env.locals.master_prefix
  infa                  = "sg"
}

#To inherit this configuration, in each of the child terragrunt.hcl files, 
#You can tell Terragrunt to automatically include all the settings from the root terragrunt.hcl file as follows:
#Flow the doccument: https://terragrunt.gruntwork.io/docs/features/keep-your-remote-state-configuration-dry/
include "root" {
  path = find_in_parent_folders()
}


# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  source = "../../../../modules/jarvis//terraform-aws-sg"
}

dependency vpc {
  config_path = "../vpc"
}

# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {

  name_bastion_host_sg = "${local.master_prefix}-${local.environment}-bastion-host"
  name_api_sg          = "${local.master_prefix}-${local.environment}-api"

  vpc_id             = dependency.vpc.outputs.vpc
  map_subnet_name_id = dependency.vpc.outputs.map_subnet_name_id


  tags = {
    "Terraform" : true
    "vtc:environment-name" : local.environment
    "vtc:aws-region" : local.aws_region
    "vtc:infa" : local.infa
  }
}
