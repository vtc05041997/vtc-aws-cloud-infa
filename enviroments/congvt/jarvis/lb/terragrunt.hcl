locals {
  var_env = read_terragrunt_config(find_in_parent_folders("var_env.hcl"))

  # Extract out common variables for reuse
  account_name          = local.var_env.locals.account_name
  aws_account_id        = local.var_env.locals.aws_account_id
  aws_profile           = local.var_env.locals.aws_profile
  aws_iam_sso_role_name = local.var_env.locals.aws_iam_sso_role_name
  environment           = local.var_env.locals.environment
  environment_code      = local.var_env.locals.environment_code
  aws_region            = local.var_env.locals.aws_region
  aws_region_code       = local.var_env.locals.aws_region_code
  database_prefix       = local.var_env.locals.database_prefix
  master_prefix         = local.var_env.locals.master_prefix
  infa                  = "lb"

}

#To inherit this configuration, in each of the child terragrunt.hcl files, 
#You can tell Terragrunt to automatically include all the settings from the root terragrunt.hcl file as follows:
#Flow the doccument: https://terragrunt.gruntwork.io/docs/features/keep-your-remote-state-configuration-dry/
include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "../../../../modules/jarvis//terraform-aws-lb"
}

dependency vpc {
  config_path = "../vpc"
}

dependency sg {
  config_path = "../sg"
}


# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {

  vpc_id          = dependency.vpc.outputs.vpc
  security_groups = [dependency.sg.outputs.security_group_api_id]
  public_subnets  = dependency.vpc.outputs.public_subnets
  private_subnets = dependency.vpc.outputs.private_subnets
  env             = local.environment

  tags = {
    "Terraform" : true
    "vtc:environment-name" : local.environment
    "vtc:aws-region" : local.aws_region
    "vtc:infa" : local.infa
  }
}
