locals {
  var_env = read_terragrunt_config(find_in_parent_folders("var_env.hcl"))

  # Extract out common variables for reuse
  account_name          = local.var_env.locals.account_name
  aws_account_id        = local.var_env.locals.aws_account_id
  aws_profile           = local.var_env.locals.aws_profile
  aws_iam_sso_role_name = local.var_env.locals.aws_iam_sso_role_name
  environment           = local.var_env.locals.environment
  environment_code      = local.var_env.locals.environment_code
  aws_region            = local.var_env.locals.aws_region
  aws_region_code       = local.var_env.locals.aws_region_code
  database_prefix       = local.var_env.locals.database_prefix
  master_prefix         = local.var_env.locals.master_prefix
  infa                  = "vpc"

}

#To inherit this configuration, in each of the child terragrunt.hcl files, 
#You can tell Terragrunt to automatically include all the settings from the root terragrunt.hcl file as follows:
#Flow the doccument: https://terragrunt.gruntwork.io/docs/features/keep-your-remote-state-configuration-dry/
include "root" {
  path = find_in_parent_folders()
}

// terraform {
//   backend "s3" {
//     bucket  = "terraform-state-congvt-ap-southeast-1"
//     key     = "enviroments/congvt/jarvis/vpc/terraform.tfstate"
//     profile = "terraform"
//     region  = "ap-southeast-1"
//   }
// }

# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  source = "../../../../modules/jarvis//terraform-aws-vpc"
}

# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {

  name = "${local.master_prefix}-${local.environment}"

  cidr_block = "10.0.0.0/16"

  # Create public subnet
  public_subnet_cidrs = ["10.0.0.0/24", "10.0.1.0/24"]
  public_subnet_name = [
    "${local.master_prefix}-${local.environment}-public-subnet-a",
    "${local.master_prefix}-${local.environment}-public-subnet-b"
  ]
  public_subnet_az = [
    "${local.aws_region}a",
    "${local.aws_region}b"
  ]

  # Create private subnet
  private_subnet_cidrs = ["10.0.16.0/20", "10.0.32.0/20"]
  private_subnet_name = [
    "${local.master_prefix}-${local.environment}-private-subnet-a",
    "${local.master_prefix}-${local.environment}-private-subnet-b"
  ]
  private_subnet_az = [
    "${local.aws_region}a",
    "${local.aws_region}b"
  ]

  map_public_ip_on_launch = true
  enable_dns_hostnames    = true
  enable_dns_support      = true

  tags = {
    "Terraform" : true
    "vtc:environment-name" : local.environment
    "vtc:aws-region" : local.aws_region
    "vtc:infa" : local.infa
  }
}
