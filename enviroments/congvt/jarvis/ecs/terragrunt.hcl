locals {
  var_env = read_terragrunt_config(find_in_parent_folders("var_env.hcl"))

  # Extract out common variables for reuse
  account_name          = local.var_env.locals.account_name
  aws_account_id        = local.var_env.locals.aws_account_id
  aws_profile           = local.var_env.locals.aws_profile
  aws_iam_sso_role_name = local.var_env.locals.aws_iam_sso_role_name
  environment           = local.var_env.locals.environment
  environment_code      = local.var_env.locals.environment_code
  aws_region            = local.var_env.locals.aws_region
  aws_region_code       = local.var_env.locals.aws_region_code
  database_prefix       = local.var_env.locals.database_prefix
  master_prefix         = local.var_env.locals.master_prefix
  infa                  = "ecs"

}

#To inherit this configuration, in each of the child terragrunt.hcl files, 
#You can tell Terragrunt to automatically include all the settings from the root terragrunt.hcl file as follows:
#Flow the doccument: https://terragrunt.gruntwork.io/docs/features/keep-your-remote-state-configuration-dry/
include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "../../../../modules/jarvis//terraform-aws-ecs"
}

dependency autoscaling {
  config_path = "../auto-scaling"
}

dependency iam {
  config_path = "../iam"
}

dependency ecr {
  config_path = "../ecr"
}

dependency sg {
  config_path = "../sg"
}

dependency lb {
  config_path = "../lb"
}

dependency vpc {
  config_path = "../vpc"
}

dependency clw {
  config_path = "../cloud-watch"
}

# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {

  security_groups = [dependency.sg.outputs.security_group_api_id]
  env             = local.environment


  vtc_stormbreaker_api_asg_arn     = dependency.autoscaling.outputs.vtc_stormbreaker_api_asg_arn
  iam_role_ecs_task_execution_arn  = dependency.iam.outputs.ecs_task_execution_role_arn
  vtc_stormbreaker_api_ecr         = dependency.ecr.outputs.vtc_stormbreaker_api_ecr
  vtc_stormbreaker_api_lb_tg       = dependency.lb.outputs.vtc_stormbreaker_api_tg
  public_subnets                   = dependency.vpc.outputs.public_subnets
  private_subnets                  = dependency.vpc.outputs.private_subnets
  vtc_stormbreaker_api_cwl_gr_arn  = dependency.clw.outputs.vtc_stormbreaker_api_cwl_gr_arn
  vtc_stormbreaker_api_cwl_gr_name = dependency.clw.outputs.vtc_stormbreaker_api_cwl_gr_name

  tags = {
    "Terraform" : true
    "vtc:environment-name" : local.environment
    "vtc:aws-region" : local.aws_region
    "vtc:infa" : local.infa
  }
}
