locals {
  var_env = read_terragrunt_config(find_in_parent_folders("var_env.hcl"))

  # Extract out common variables for reuse
  account_name          = local.var_env.locals.account_name
  aws_account_id        = local.var_env.locals.aws_account_id
  aws_profile           = local.var_env.locals.aws_profile
  aws_iam_sso_role_name = local.var_env.locals.aws_iam_sso_role_name
  environment           = local.var_env.locals.environment
  environment_code      = local.var_env.locals.environment_code
  aws_region            = local.var_env.locals.aws_region
  aws_region_code       = local.var_env.locals.aws_region_code
  database_prefix       = local.var_env.locals.database_prefix
  master_prefix         = local.var_env.locals.master_prefix
  infa                  = "template"

}

#To inherit this configuration, in each of the child terragrunt.hcl files, 
#You can tell Terragrunt to automatically include all the settings from the root terragrunt.hcl file as follows:
#Flow the doccument: https://terragrunt.gruntwork.io/docs/features/keep-your-remote-state-configuration-dry/
include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "../../../../modules/jarvis//terraform-aws-template"
}

dependency sg {
  config_path = "../sg"
}

dependency iam {
  config_path = "../iam"
}


# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {

  security_groups = [dependency.sg.outputs.security_group_api_id]
  env             = local.environment

  instance_keypair = "key-for-ec2"
  instance_type    = "t2.micro"

  # Normal image_id
  # image_id         = "ami-06018068a18569ff2"
  
  # For ecs
  image_id         = "ami-0b00532b21d4d18e4"
  application_type = "api"

  aws_iam_instance_profile_arn = dependency.iam.outputs.aws_iam_instance_profile_arn

  tags = {
    "Terraform" : true
    "vtc:environment-name" : local.environment
    "vtc:aws-region" : local.aws_region
    "vtc:infa" : local.infa
  }
}
