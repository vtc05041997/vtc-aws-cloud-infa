locals {
  # Automatically load environment-level variables
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  account_vars     = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  region_vars      = read_terragrunt_config(find_in_parent_folders("region.hcl"))

  # Extract out common variables for reuse
  env                   = local.environment_vars.locals.environment
  aws_account_id        = local.account_vars.locals.aws_account_id
  aws_iam_sso_role_name = local.account_vars.locals.aws_iam_sso_role_name
  aws_region            = local.region_vars.locals.aws_region
  aws_region_code       = local.region_vars.locals.aws_region_code

  # Including globally defined locals
  common_vars = yamldecode(file(find_in_parent_folders("common_vars.yaml")))
}

# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  source = "../../../modules/projects/delivery-elt/delivery-vtc-v1//terraform-aws-glue"
}

#To inherit this configuration, in each of the child terragrunt.hcl files, 
#You can tell Terragrunt to automatically include all the settings from the root terragrunt.hcl file as follows:
#Flow the doccument: https://terragrunt.gruntwork.io/docs/features/keep-your-remote-state-configuration-dry/
include "root" {
  path = find_in_parent_folders()
}


dependency s3 {
   config_path = "../s3"
}

dependency iam {
  config_path = "../sg"
}

# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {
  master_prefix   = local.common_vars.master_prefix
  env_prefix      = local.env
  database_prefix = local.common_vars.database_prefix

  namespace_to_s3bucketname = {
    "raw_bucket_name" : "vtc-data-${local.env}-raw-${local.aws_region}-${local.aws_account_id}"
    "insight_bucket_name": "vtc-data-${local.env}-insight-${local.aws_region}-${local.aws_account_id}"
  }


  tags = {
    "Terraform" : true
    "tcb:environment-name" : local.env
  }
}
